package com.learn.apache.camel.basic;

import org.apache.camel.builder.RouteBuilder;

public class RoutingToDifferentDestinations extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file://source")
			.choice()
            	.when(simple("${in.header.CamelFileName} contains 'widget.txt'"))
            		.to("file://widget")
            	.when(simple("${in.header.CamelFileName} contains 'gadget.txt'"))
            		.to("file://gadget")
            	.otherwise()
            		.to("log://com.learn.apache.camel.basic");
	}

}
