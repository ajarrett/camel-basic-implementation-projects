package com.learn.apache.camel.basic;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Before;
import org.junit.Test;

public class RoutingToDifferentDestinationsRouteTest extends CamelTestSupport {
	
    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RoutingToDifferentDestinations();
    }
	
	@EndpointInject(uri = "mock:file:gadget")
	private MockEndpoint mockGadget;
	
    @EndpointInject(uri = "mock:file:widget")
    private MockEndpoint mockWidget;
    
    @EndpointInject(uri = "mock:log:com.learn.apache.camel.basic")
    private MockEndpoint mockLog;
    
    @Produce(uri = "direct:start")
    protected ProducerTemplate start;
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        context.getRouteDefinitions().get(0).adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                replaceFromWith("direct:start");
                mockEndpointsAndSkip("*");
            }
        });
    }
    
    @Test
    public void sendsAWidgetMessage() throws Exception {
        mockWidget.expectedBodiesReceived("Test message");
        mockGadget.expectedMessageCount(0);
        mockLog.expectedMessageCount(0);

        start.sendBodyAndHeader("Test message", "CamelFileName", "widget.txt");
        assertMockEndpointsSatisfied();
    }
    
    @Test
    public void sendsAGadgetMessage() throws Exception {
        mockWidget.expectedMessageCount(0);
        mockGadget.expectedBodiesReceived("Test message");
        mockLog.expectedMessageCount(0);

        start.sendBodyAndHeader("Test message", "CamelFileName", "gadget.txt");
        assertMockEndpointsSatisfied();
    }
    
    @Test
    public void sendsAGadgetAndWidgetMessage() throws Exception {
        mockWidget.expectedBodiesReceived("Widget Message");
        mockGadget.expectedBodiesReceived("Gadget Message");
        mockLog.expectedMessageCount(0);

        start.sendBodyAndHeader("Gadget Message", "CamelFileName", "gadget.txt");
        start.sendBodyAndHeader("Widget Message", "CamelFileName", "widget.txt");
        assertMockEndpointsSatisfied();
    }
    
    @Test
    public void sendsAOtherMessage() throws Exception {
        mockWidget.expectedMessageCount(0);
        mockGadget.expectedMessageCount(0);
        mockLog.expectedBodiesReceived("Test message");

        start.sendBodyAndHeader("Test message", "CamelFileName", "other.txt");
        assertMockEndpointsSatisfied();
    }

}
