package com.learn.apache.camel.basic;

import org.apache.camel.builder.RouteBuilder;

public class UsingComponentsRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		
		from("jetty:http://localhost:8181?matchOnUriPrefix=true")
        .to("log:com.learn.apache.camel.basic?showAll=true&multiline=true")
        .to("jetty:http://www.google.co.uk?bridgeEndpoint=true&throwExceptionOnFailure=false");
		
	}

}
