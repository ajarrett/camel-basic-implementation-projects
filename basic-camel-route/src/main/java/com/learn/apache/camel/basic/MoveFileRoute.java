package com.learn.apache.camel.basic;

import org.apache.camel.builder.RouteBuilder;

public class MoveFileRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file://source") 
			.to("log://com.learn.apache.camel.basic?showAll=true")
			.to("file://target");
		
	}

}
