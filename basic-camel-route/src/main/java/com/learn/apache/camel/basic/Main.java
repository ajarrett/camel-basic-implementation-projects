package com.learn.apache.camel.basic;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class Main {

	public static void main(String[] args) throws Exception {
		CamelContext camelContext = new DefaultCamelContext();
		camelContext.addRoutes(new MoveFileRoute());
		camelContext.start();
		Thread.sleep(10000);
		camelContext.stop();
	}

}
